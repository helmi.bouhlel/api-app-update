<?php
/**
 * Created by PhpStorm.
 * User: helmi
 * Date: 31/03/2019
 * Time: 12:00 PM
 */

namespace App\Service;


use phpDocumentor\Reflection\Types\This;

class HTML2PDF
{
    private $pdf;
    public function create ($oriontation = null,$format = null,$lang = null,$unicode= null,$encoding = null,$marging = null ){
        $this->pdf = new \Spipu\Html2Pdf\Html2Pdf(
            $oriontation ? $oriontation: $this->oriontation,
            $format ? $format : $this->format,
            $lang ? $lang : $this->lang,
            $unicode ? $unicode : $this->unicode,
            $encoding ? $encoding : $this->encoding,
            $marging ? $marging : $this->marging
         );
    }


    public function  generatePDF($template,$name){
        $this->pdf->writeHTML($template);
        return $this->pdf->Output($name.'.pdf');
    }

}
