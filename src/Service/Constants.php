<?php
/**
 * Created by PhpStorm.
 * User: helmi
 * Date: 29/03/2019
 * Time: 10:35 PM
 */

namespace App\Service;


class Constants
{
    /**********CODE TYPE REFERENTIELS***********/
    const  REF_CODE_CAT_PRODUIT = "cat-produit";
    const  REF_CODE_TAILLE = "";
    const  REF_CODE_MESURE = "mesure-produit";
    const  REF_CODE_PROVINCE = "province-code";
    const  REF_CODE_ETAT_BL = "etat-livraison";
    const  PRIX_LIVRAISON = 7;

}
