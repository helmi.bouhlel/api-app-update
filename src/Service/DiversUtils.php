<?php
/**
 * Created by PhpStorm.
 * User: helmi
 * Date: 24/02/2019
 * Time: 3:47 PM
 */

namespace App\Service;
use App\Entity\ReferentielsType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DiversUtils extends Controller
{

    public function getByCode (String $code): array
    {
        $repository = $this->getDoctrine()->getRepository(ReferentielsType::class);
        $referentielsType = $repository->findOneBy(['code' => $code]);

        $arrayResult = array();
        array_push($arrayResult, $referentielsType->getId());
        foreach ($referentielsType->getFils() as $referentielsTypeFils) {
            array_push($arrayResult, $referentielsTypeFils->getId());
            $arrFils = $repository->find($referentielsTypeFils->getId())->getFils();
            foreach ($arrFils as $referentielsSousFils) {
                array_push($arrayResult, $referentielsSousFils->getId());
            }
        }
        return $arrayResult;
    }

}
