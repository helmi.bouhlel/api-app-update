<?php


namespace App\DTO;


use Symfony\Component\Serializer\Annotation\SerializedName;

class ElementDTO
{

    private $title;
    private $image_url;
    private $subtitle;
    private $buttons;

    public function __construct(array $buttons,string $subtitle,string $image_url)
    {
        $this->buttons = $buttons;

        $this->title = $buttons[0]->getTitle();
        $this->image_url = "http://mg-shop.tn/media/cache/small_image/uploads/images/products/".$image_url;
        $this->subtitle = $subtitle;
    }

    /**
     * @return mixed
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param mixed $title
     */
    public function setTitle($title): void
    {
        $this->title = $title;
    }

    /** @SerializedName("image_url") */
    public function getImageUrl()
    {
        return $this->image_url;
    }

    /**
     * @param mixed $image_url
     */
    public function setImageUrl($image_url): void
    {
        $this->image_url = $image_url;
    }

    /**
     * @return mixed
     */
    public function getSubtitle()
    {
        return $this->subtitle;
    }

    /**
     * @param mixed $subtitle
     */
    public function setSubtitle($subtitle): void
    {
        $this->subtitle = $subtitle;
    }

    /**
     * @return array
     */
    public function getButtons(): array
    {
        return $this->buttons;
    }

    /**
     * @param array $buttons
     */
    public function setButtons(array $buttons): void
    {
        array_push($this->buttons,$buttons)  ;
    }


}
