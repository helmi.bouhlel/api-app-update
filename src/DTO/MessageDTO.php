<?php


namespace App\DTO;


class MessageDTO
{
    private $attachment;

    public function __construct()
    {
        $this->attachment = new AttachmentDTO();
    }

    /**
     * @return mixed
     */
    public function getAttachment():AttachmentDTO
    {
        return $this->attachment;
    }

    /**
     * @param mixed $attachment
     */
    public function setAttachment(AttachmentDTO $attachment): void
    {
        $this->attachment = $attachment;
    }


}
