<?php


namespace App\DTO;


class ButtonDTO
{

    private $type;
    private $url;
    private $title;

    public function __construct(string $title, string $id)
    {
        $this->type = "web_url";
        $this->url = "http://mg-shop.tn/produitDetail?id=".$id;
        $this->title = $title;

    }

    /**
     * @return mixed
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param mixed $type
     */
    public function setType($type): void
    {
        $this->type = $type;
    }

    /**
     * @return mixed
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * @param mixed $url
     */
    public function setUrl($url): void
    {
        $this->url = $url;
    }

    /**
     * @return mixed
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param mixed $title
     */
    public function setTitle($title): void
    {
        $this->title = $title;
    }


}
