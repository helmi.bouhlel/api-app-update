<?php


namespace App\DTO;


use Symfony\Component\Serializer\Annotation\SerializedName;

class PayloadDTO
{
    private $template_type;
    private $image_aspect_ratio;
    private $elements;

    public function __construct()
    {
        $this->template_type = "generic";
        $this->image_aspect_ratio = "square";
        $this->elements = array();
    }

    /** @SerializedName("template_type") */
    public function getTemplateType()
    {
        return $this->template_type;
    }

    /**
     * @param mixed $template_type
     */
    public function setTemplateType($template_type): void
    {
        $this->template_type = $template_type;
    }

    /** @SerializedName("image_aspect_ratio") */
    public function getImageAspectRatio()
    {
        return $this->image_aspect_ratio;
    }

    /**
     * @param mixed $image_aspect_ratio
     */
    public function setImageAspectRatio($image_aspect_ratio): void
    {
        $this->image_aspect_ratio = $image_aspect_ratio;
    }

    /**
     * @return array
     */
    public function getElements(): array
    {
        return $this->elements;
    }

    /**
     * @param array $elements
     */
    public function setElements(array $elements): void
    {
        $this->elements = $elements;
    }




}
