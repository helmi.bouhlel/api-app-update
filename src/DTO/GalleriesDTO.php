<?php


namespace App\DTO;


class GalleriesDTO
{
    private  $message;

    /**
     * GalleriesDTO constructor.
     * @param $message
     */
    public function __construct()
    {
        $this->message = array(MessageDTO::class);
    }


    /**
     * @return mixed
     */
    public function getMessage(): MessageDTO
    {
        return $this->message;
    }

    /**
     * @param mixed $message
     */
    public function setMessage(MessageDTO $message): void
    {
        $this->message = $message;
    }



}
