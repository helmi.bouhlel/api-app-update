<?php


namespace App\DTO;


class AttachmentDTO
{
    private $type;
    private $payload;

    public function __construct()
    {
        $this->payload = new PayloadDTO();
        $this->type = "template";
    }

    /**
     * @return mixed
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param mixed $type
     */
    public function setType($type): void
    {
        $this->type = $type;
    }

    /**
     * @return mixed
     */
    public function getPayload():PayloadDTO
    {
        return $this->payload;
    }

    /**
     * @param mixed $payload
     */
    public function setPayload(PayloadDTO $payload): void
    {
        $this->payload = $payload;
    }



}
