<?php

namespace App\Controller;

use App\DTO\ButtonDTO;
use App\DTO\Chatfuel;
use App\DTO\ElementDTO;
use App\DTO\MessageDTO;
use App\Entity\BondeL;
use App\Entity\Client;
use App\Entity\DetailBondeL;
use App\Entity\Images;
use App\Entity\Produit;
use App\Entity\Referentiels;
use App\Entity\ReferentielsType;
use App\Entity\Stock;
use App\Form\ClientLivraisonForm;
use App\Service\Constants;
use Doctrine\Common\Annotations\AnnotationReader;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Security\Csrf\CsrfTokenManagerInterface;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Encoder\XmlEncoder;
use Symfony\Component\Serializer\Mapping\Factory\ClassMetadataFactory;
use Symfony\Component\Serializer\Mapping\Loader\AnnotationLoader;
use Symfony\Component\Serializer\NameConverter\MetadataAwareNameConverter;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;

class DefaultController extends AbstractController
{
    private $tokenManager;

    public function __construct(CsrfTokenManagerInterface $tokenManager = null)
    {
        $this->tokenManager = $tokenManager;
    }

    public function logAction(Request $request)
    {
        /** @var $session Session */
        $session = $request->getSession();

        $authErrorKey = Security::AUTHENTICATION_ERROR;
        $lastUsernameKey = Security::LAST_USERNAME;

        // get the error if any (works with forward and redirect -- see below)
        if ($request->attributes->has($authErrorKey)) {
            $error = $request->attributes->get($authErrorKey);
        } elseif (null !== $session && $session->has($authErrorKey)) {
            $error = $session->get($authErrorKey);
            $session->remove($authErrorKey);
        } else {
            $error = null;
        }

        if (!$error instanceof AuthenticationException) {
            $error = null; // The value does not come from the security component.
        }

        // last username entered by the user
        $lastUsername = (null === $session) ? '' : $session->get($lastUsernameKey);

        $csrfToken = $this->tokenManager
            ? $this->tokenManager->getToken('authenticate')->getValue()
            : null;

        return [
            'last_username' => $lastUsername,
            'error' => $error,
            'csrf_token' => $csrfToken,
        ];
    }

    /**
     * @Route("/", name="index")
     */
    public function index(Request $request)
    {
        $repositoryRef = $this->getDoctrine()->getRepository(ReferentielsType::class);
        $ref = $repositoryRef->findOneBy(['code' => Constants::REF_CODE_CAT_PRODUIT]);
        //die(dump($ref->getFils()->first()));

        $refPopulaire = $this->getDoctrine()->getRepository(Referentiels::class)
        ->findBy(['popular' => true]);

        $produitPopulaire = $repositoryRef = $this->getDoctrine()->getRepository(Produit::class)
        ->findBy(['popular' => true]);

        return $this->render('page/index.html.twig',
           array_merge(['refType' => $ref, 'refPopulaire' => $refPopulaire, 'produitPopulaire' => $produitPopulaire], $this->logAction($request)));
    }

    /**
     * @Route("/recherche", name="recherchePost" ,methods={"POST"})
     */
    public function recherchePost(Request $request)
    {
        $search = $request->request->get('search', 'default category');
        if ($search) {
            $produitResult = $this->getDoctrine()->getRepository(Produit::class)->searchText($search);
        }
        $repositoryRef = $this->getDoctrine()->getRepository(ReferentielsType::class);
        $ref = $repositoryRef->findOneBy(['code' => Constants::REF_CODE_CAT_PRODUIT]);

        return $this->render('page/recherche.html.twig', array_merge(
            $this->logAction($request), ['refType' => $ref, 'produitResult' => $produitResult, 'isSolde' => false]
        )
            );
    }

    /**
     * @Route("/recherche", name="recherche" , methods={"GET"})
     */
    public function recherche(Request $request)
    {
        $idRef = $request->query->get('idRef');
        $idTypeRef = $request->query->get('idTypeRef');
        $trie = $request->query->get('trie');
        $isSolde = false;

        $repositoryRef = $this->getDoctrine()->getRepository(Produit::class);
        if ($idRef) {
            $produitResult = $repositoryRef->findBy(['referentiels' => $idRef]);
        //die(dump($produitResult));
        } elseif ($idTypeRef) {
            $produitResult = $repositoryRef->getByCode($idTypeRef);
        //die(dump($produitResult));
        } else {
            $produitResult = $repositoryRef->findProduitSolder();
            $isSolde = true;
        }
        $repositoryRef = $this->getDoctrine()->getRepository(ReferentielsType::class);
        $ref = $repositoryRef->findOneBy(['code' => Constants::REF_CODE_CAT_PRODUIT]);

        if ('PC' == $trie) {
            usort($produitResult, function ($a, $b) {return strcmp($b->getPrixHT(), $a->getPrixHT()); });
        } elseif ('MC' == $trie) {
            usort($produitResult, function ($a, $b) {return strcmp($a->getPrixHT(), $b->getPrixHT()); });
        } elseif ('PD' == $trie) {
        }

        return $this->render('page/recherche.html.twig', array_merge(
            $this->logAction($request),
            ['refType' => $ref, 'produitResult' => $produitResult, 'isSolde' => $isSolde]));
    }

    /**
     * @Route("/produitDetail", name="produitDetail")
     */
    public function produitDetail(Request $request)
    {
        $id = $request->query->get('id');
        if ($id) {
            $repository = $this->getDoctrine()->getRepository(Produit::class);
            $produit = $repository->findOneBy(['id' => $id]);
            //die(dump($produit));

            $encoders = [new XmlEncoder(), new JsonEncoder()];
            $normalizers = [new ObjectNormalizer()];
            $serializer = new Serializer($normalizers, $encoders);
//
//            $jsonProduit = $jsonContent = $serializer->serialize($produit, 'json', [
//                'circular_reference_handler' => function ($object) {
//                    return $object->getId();
//                },'enable_max_depth' => true
//            ]);

            // dump($produit);
            return $this->render('page/produit-detail.html.twig', array_merge(
                $this->logAction($request),
                ['produit' => $produit /* , 'produitJson'=> $jsonProduit */]));
        } else {
            return $this->index($request);
        }
    }

    /**
     * @Route("/sendMail", name="sendMail")
     */
    public function sendEmail(Request $request, \Swift_Mailer $mailer)
    {
        $message = (new \Swift_Message('Hello Email'))
            ->setFrom('noreply@mg-shop.tn')
            ->setTo('helmi.bouh@gmail.com')
            ->setBody('You should see me from the profiler!')
        ;

        return $this->json($mailer->send($message));
    }

    /**
     * @Route("/bacth", name="bacth")
     */
    public function bacth(Request $request)
    {
        $produits = $this->getDoctrine()->getRepository(Produit::class)->findAll();

        $entityManager = $this->getDoctrine()->getManager();

        foreach ($produits as $p) {
            if ($p->getImages()->isEmpty() || null == $p->getImages()) {
                if (null != $p->getImg()) {
                    $image = new Images();
                    $image->setVisible(1);
                    $image->setFileName($p->getImg());
                    $image->setLibelle($p->getImg());
                    $image->setProduit($p);
                    $image2 = $image;
                    $entityManager->persist($image);
                    $entityManager->persist($image2);
                    $entityManager->flush();
                }
            }
        }
    }

    /**
     * @Route("/checkout", name="checkout")
     */
    public function checkout(Request $request)
    {
        $client = new Client();
        $form = $this->createForm(ClientLivraisonForm::class, $client);

        $repositoryRef = $this->getDoctrine()->getRepository(ReferentielsType::class);
        $repositorStock = $this->getDoctrine()->getRepository(Stock::class);
        $repositorProd = $this->getDoctrine()->getRepository(Produit::class);
        $ref = $repositoryRef->findOneBy(['code' => Constants::REF_CODE_PROVINCE]);
        $reff = $this->getDoctrine()->getRepository(Referentiels::class)->findBy(
            ['referentielsType' => $ref->getId()]);
        //dump($reff );
        if ('POST' == $request->getMethod()) {
            $form->handleRequest($request);

            $result = json_decode($client->getPannierJson(), true);
            $c = $result[0];
            //die(dump($result[0])) ;
            $bondeL = new BondeL();
            $bondeL->setDateCre(new \DateTime());
            $bondeL->setDateL(new \DateTime());
            $bondeL->setClient($client);
            $bondeL->setLivrer(false);
            $bondeL->setPrixHT(Constants::PRIX_LIVRAISON);
            foreach ($result as $res) {
                $prixHT = $res['produit']['prixHT'];
                $solde = $res['produit']['solde'];
                $qte = $res['stock']['qteLivrer'];

                $stock = $repositorStock->find($res['stock']['id']);
                $produit = $repositorProd->find($res['produit']['id']);

                $detBL = new DetailBondeL();
                $detBL->setMesure($stock->getMesure());
                $detBL->setQte($qte);
                $detBL->setProduit($produit);
                if (null != $res['produit']['solde']) {
                    $detBL->setPrixHT(($prixHT - (($prixHT / 100) * $solde)) * $qte);
                } else {
                    $detBL->setPrixHT($prixHT * $qte);
                }
                $bondeL->setPrixHT($bondeL->getPrixHT() + $detBL->getPrixHT());
                $bondeL->addDetailBondeL($detBL);
            }

            //die(dump($bondeL));
            if ($form->isSubmitted() && $form->isValid()) {
                $entityManager = $this->getDoctrine()->getManager();
                $entityManager->persist($bondeL);
                $entityManager->flush();
                $this->addFlash('success', 'Votre commande a été effectué avec succès. Actuellement, votre commande est en cours de traitement ');

                return $this->redirectToRoute('index');
            }
            //dump($bondeL);
            //return $this->index($request);
        }

        return $this->render('page/checkout.html.twig', array_merge(
            $this->logAction($request), ['governorat' => $reff, 'form' => $form->createView()]));
    }

    /**
     * @Route("/contact", name="contact")
     */
    public function contact(Request $request)
    {
        return $this->render('page/contact.html.twig', array_merge(
            $this->logAction($request), []));
    }

    /**
     * @Route("/livraison", name="livrerAction" ,methods={"POST"})
     */
    public function livraison(Request $request)
    {
        die(dump($request));
    }

    /**
     * @Route("/getProduitSolder", name="getProduitSolder" ,methods={"GET"})
     */
    public function getProduitSolder(Request $request)
    {
        $repositoryRef = $this->getDoctrine()->getRepository(Produit::class);
        $produits = $this->getDoctrine()->getRepository(Produit::class)->findAll();
        $produitsSolder = $repositoryRef->findProduitSolder();
        $messageResult = new MessageDTO();
        $elements = [];

        $i = 0;
        foreach ($produitsSolder as $p) {

            $button = new ButtonDTO($p->getLibelle(), $p->getId());
            $buttons = [];
            array_push($buttons, $button);
            // dump($button);

            $element = new ElementDTO($buttons, $p->getPrixHTAchat(), $p->getImages()->get(0)->getFileName());
            array_push($elements, $element);
            $messageResult->getAttachment()->getPayload()->setElements($elements);

            $element = null;
            ++$i;
        }
        //die(dump($messageResult));

        $messages = [$messageResult];
        $chatfuel = new Chatfuel($messages);

        $classMetadataFactory = new ClassMetadataFactory(new AnnotationLoader(new AnnotationReader()));

        $metadataAwareNameConverter = new MetadataAwareNameConverter($classMetadataFactory);

        $serializer = new Serializer(
            [new ObjectNormalizer($classMetadataFactory, $metadataAwareNameConverter)],
            ['json' => new JsonEncoder()]
        );
        $serialized = $serializer->serialize($chatfuel, 'json');

        //$serializedEntity = $this->container->get('serializer')->serialize($chatfuel);

        return new Response($serialized);

        //return $this->json($repositoryRef->findProduitSolder());

        /*$encoders = [new JsonEncoder()]; // If no need for XmlEncoder
        $normalizers = [new ObjectNormalizer()];
        $serializer = new Serializer($normalizers, $encoders);

        // Serialize your object in Json
        $jsonObject = $serializer->serialize($repositoryRef->findProduitSolder(), 'json', [
            'circular_reference_handler' => function ($object) {
                return $object->getId();
            }
        ]);

        // For instance, return a Response with encoded Json
        return new Response($jsonObject, 200, ['Content-Type' => 'application/json']);
        */
    }
}
