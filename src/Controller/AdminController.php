<?php
/**
 * Created by PhpStorm.
 * User: helmi
 * Date: 17/01/2019
 * Time: 12:41 AM
 */

namespace App\Controller;

use App\Entity\BondeC;
use App\Entity\BondeL;
use App\Entity\Client;
use App\Entity\DetailBondeC;
use App\Entity\DetailBondeL;
use App\Entity\Stock;
use App\Entity\User;
use App\Repository\ReferentielsRepository;
use App\Service\Constants;

use App\Service\CsvExporter;
use FOS\UserBundle\Doctrine\UserManager;
use FOS\UserBundle\Model\UserManagerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AdminController as BaseAdminController;
use AlterPHP\EasyAdminExtensionBundle\Controller\EasyAdminController as ExtEasyController;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Encoder\EncoderFactory;
use Symfony\Component\Security\Core\Encoder\EncoderFactoryInterface;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class AdminController extends ExtEasyController
{
    /**
     * @var UserPasswordEncoderInterface
     */
    private $UserManager;

    /**
     * @var UserPasswordEncoderInterface
     */
    private $Encoder_factory;

    private $csvExporter;

    private $userM;

    /**
     * UserController constructor.
     *
     * @param UserPasswordEncoderInterface $passwordEncoder
     */
    public function __construct(UserManagerInterface $UserManager, EncoderFactoryInterface $Encoder_factory, CsvExporter $csvExporter, UserManagerInterface $userM)
    {
        $this->UserManager = $UserManager;
        $this->Encoder_factory = $Encoder_factory;
        $this->csvExporter = $csvExporter;
        $this->userM = $userM;
    }
//    public function indexAction(Request $request)
//    {
//
//        if ($request->query->get('action')==='edit' &&
//            $request->query->get('entity')==='BondeL' &&
//            $request->query->get('property')==='livrer' &&
//            $request->query->get('view')==='list') {
//            $repository = $this->getDoctrine()->getRepository(BondeL::class);
//
//            $id = $request->query->get('id');
//            $this->updateBondeLEntity($repository->find($id));
//            return new Response('lol',201);
//        }else {
//            parent::indexAction($request);
//        }
//        return parent::indexAction($request);
//    }

    public function log(string $text, LoggerInterface $logger)
    {
        $logger->info("***********************");
        $logger->debug($text);
        $logger->info("***********************");
    }

//    public function updateEntity($entity)
//    {
//        if (method_exists($entity, 'setLivrer')) {
//            $this->updateBondeLEntity($entity);
//        }else {
//            parent::updateEntity($entity);
//        }
//
//    }

    public function imprimeAction(Request $request)
    {
        parent::indexAction($request);
        //die(dump($id = $request->query->get('id')));
        $id = $request->query->get('id');
        if ($id != null) {
            $bondeL = $this->getDoctrine()->getRepository(BondeL::class)->find($id);
//
//            $html2pdf = new \Spipu\Html2Pdf\Html2Pdf('P', 'A4', 'en');
//            $html2pdf->writeHTML($this->renderView('etat/detailBbondeL.html.twig'),array('bondeL' => $bl));
//            $html2pdf->output('test.pdf');

            $template = $this->renderView('etat/detailBbondeL.html.twig', [
                'bondeL' => $bondeL
            ]);
            $html2Pdf = $this->get('app.html2pdf');
            $html2Pdf->create('P', 'A4', 'fr', true, 'UTF-8', array(10, 15, 10, 15));
            return $html2Pdf->generatePDF($template, 'test');


        }


    }

    public function persistBondeCEntity(BondeC $entity)
    {

        $repositoryStock = $this->getDoctrine()->getRepository(Stock::class);
        $entityManager = $this->getDoctrine()->getManager();

        foreach ($entity->getDetailBondeCs() as $detailBondeC) {
            $detailBondeC->setPrixHT($detailBondeC->getProduit()->getPrixHTAchat() * $detailBondeC->getQte());
            $entity->setPrixHT($entity->getPrixHT() + $detailBondeC->getPrixHT());
            $stock = new Stock();
            $stock->setMesure($detailBondeC->getMesure());
            $stock->setProduit($detailBondeC->getProduit());
            $stock->setQte($detailBondeC->getQte());
            $isStock = $repositoryStock->findOneBy([
                'produit' => $detailBondeC->getProduit()->getId(),
                'mesure' => $detailBondeC->getMesure()->getId()
            ]);
            if ($isStock) {
                $isStock->setQte($isStock->getQte() + $stock->getQte());
                $entityManager->persist($isStock);

            } else {
                $stock->setQte($detailBondeC->getQte());
                $entityManager->persist($stock);
            }


        }
        $entityManager->flush();
        parent::persistEntity($entity);
    }

    public function updateBondeCEntity(BondeC $entity)
    {
        $repositoryStock = $this->getDoctrine()->getRepository(Stock::class);
        $entityManagerDelete = $this->getDoctrine()->getManager();
        $entityManagerUpdate = $this->getDoctrine()->getManager();

        $entity->setPrixHT(0);
        if ($entity->getDetailBondeCsSupp() != null) {
            foreach ($entity->getDetailBondeCsSupp() as $detailBondeC) {
                if ($detailBondeC->getBondeC() === null) {
                    $isStock = $repositoryStock->findOneBy([
                        'produit' => $detailBondeC->getProduit()->getId(),
                        'mesure' => $detailBondeC->getMesure()->getId()
                    ]);
                    if ($isStock) {
                        $isStock->setQte($isStock->getQte() - $detailBondeC->getQte());
                        $entityManagerDelete->persist($isStock);
                    }
                }
            }
            $entityManagerDelete->flush();
        }


        foreach ($entity->getDetailBondeCs() as $detailBondeC) {
            $detailBondeC->setPrixHT($detailBondeC->getProduit()->getPrixHTAchat() * $detailBondeC->getQte());
            $entity->setPrixHT($entity->getPrixHT() + $detailBondeC->getPrixHT());
            $diff = $detailBondeC->getQte() - $detailBondeC->getOldQte();
            //die(dump($diff));
            if ($diff != 0) {
                $isStock = $repositoryStock->findOneBy([
                    'produit' => $detailBondeC->getProduit()->getId(),
                    'mesure' => $detailBondeC->getMesure()->getId()
                ]);
                if ($isStock) {
                    if ($diff > 0) {
                        $isStock->setQte($isStock->getQte() + abs($diff));
                    } else {
                        $isStock->setQte($isStock->getQte() - abs($diff));
                    }
                    $entityManagerUpdate->persist($isStock);
                } else {
                    $stock = new Stock();
                    $stock->setProduit($detailBondeC->getProduit());
                    $stock->setMesure($detailBondeC->getMesure());
                    $stock->setQte($detailBondeC->getQte());
                    $entityManagerUpdate->persist($stock);
                }
            }
        }
        $entityManagerUpdate->flush();
        parent::updateEntity($entity);
    }

    public function persistBondeLEntity(BondeL $entity)
    {
        $repositoryStock = $this->getDoctrine()->getRepository(Stock::class);
        $repositoryClient = $this->getDoctrine()->getRepository(Client::class);
        $entityManager = $this->getDoctrine()->getManager();
        foreach ($entity->getDetailBondeLs() as $detailBonde) {
            $detailBonde->setPrixHT($detailBonde->getProduit()->getPrixHT() * $detailBonde->getQte());
            $entity->setPrixHT($entity->getPrixHT() + $detailBonde->getPrixHT());
            if ($entity->getLivrer()) {
                $stock = new Stock();
                $stock->setMesure($detailBonde->getMesure());
                $stock->setProduit($detailBonde->getProduit());
                $stock->setQte($detailBonde->getQte());
                $isStock = $repositoryStock->findOneBy([
                    'produit' => $detailBonde->getProduit()->getId(),
                    'mesure' => $detailBonde->getMesure()->getId()
                ]);
                if ($isStock) {
                    $isStock->setQte($isStock->getQte() - $stock->getQte());
                    $entityManager->persist($isStock);

                } else {
                    $stock->setQte(-$detailBonde->getQte());
                    $entityManager->persist($stock);
                }

            }
        }

        $clientExiste = $repositoryClient->findOneBy(["tel1" => $entity->getClient()->getTel1()]);
        if ($clientExiste != null) {
            $entity->setClient($clientExiste);
        }

        $entityManager->flush();
        parent::persistEntity($entity);
    }

    public function updateBondeLEntity(BondeL $entity)
    {

        $repositoryStock = $this->getDoctrine()->getRepository(Stock::class);
        $entityManagerDelete = $this->getDoctrine()->getManager();
        $entityManagerUpdate = $this->getDoctrine()->getManager();

        $entity->setPrixHT(0);
        if ($entity->getDetailBondeLsSupp() != null) {
            foreach ($entity->getDetailBondeLsSupp() as $detailBondeL) {
                if ($detailBondeL->getBondeL() === null) {
                    $isStock = $repositoryStock->findOneBy([
                        'produit' => $detailBondeL->getProduit()->getId(),
                        'mesure' => $detailBondeL->getMesure()->getId()
                    ]);
                    if ($isStock) {
                        $isStock->setQte($isStock->getQte() + $detailBondeL->getQte());
                        $entityManagerDelete->persist($isStock);
                    }
                }
            }
            $entityManagerDelete->flush();
        }

        //die(dump($entity));

        if ($entity->getLivrer()) {
            foreach ($entity->getDetailBondeLs() as $detailBondeL) {
                $detailBondeL->setPrixHT($detailBondeL->getProduit()->getPrixHT() * $detailBondeL->getQte());
                $entity->setPrixHT($entity->getPrixHT() + $detailBondeL->getPrixHT());
                $diff = $detailBondeL->getOldQte() - $detailBondeL->getQte();
                // die(dump($detailBondeL->getOldQte(). ' || '. $detailBondeL->getQte()));

                if ($diff != 0) {
                    $isStock = $repositoryStock->findOneBy([
                        'produit' => $detailBondeL->getProduit()->getId(),
                        'mesure' => $detailBondeL->getMesure()->getId()
                    ]);
                    if ($isStock) {
                        if ($diff > 0) {
                            $isStock->setQte($isStock->getQte() + abs($diff));
                        } else {
                            $isStock->setQte($isStock->getQte() - abs($diff));
                        }
                        $entityManagerUpdate->persist($isStock);
                    } else {
                        $stock = new Stock();
                        $stock->setProduit($detailBondeL->getProduit());
                        $stock->setMesure($detailBondeL->getMesure());
                        $stock->setQte($detailBondeL->getQte());
                        $entityManagerUpdate->persist($stock);
                    }
                }
            }


        } else {

            foreach ($entity->getDetailBondeLs() as $detailBondeL) {
                $detailBondeL->setPrixHT($detailBondeL->getProduit()->getPrixHT() * $detailBondeL->getQte());
                $entity->setPrixHT($entity->getPrixHT() + $detailBondeL->getPrixHT());
                $diff = $detailBondeL->getQte();
                // die(dump($detailBondeL->getOldQte(). ' || '. $detailBondeL->getQte()));

                $isStock = $repositoryStock->findOneBy([
                    'produit' => $detailBondeL->getProduit()->getId(),
                    'mesure' => $detailBondeL->getMesure()->getId()
                ]);
                if ($isStock) {
                    $isStock->setQte($isStock->getQte() + ($diff));
                    $entityManagerUpdate->persist($isStock);
                } else {
                    $stock = new Stock();
                    $stock->setProduit($detailBondeL->getProduit());
                    $stock->setMesure($detailBondeL->getMesure());
                    $stock->setQte($detailBondeL->getQte());

                    $entityManagerUpdate->persist($stock);
                }

            }

        }
        $entityManagerUpdate->flush();
        parent::updateEntity($entity);
    }

    protected function createBondeLEntityFormBuilder($entity, $view)
    {
        $formBuilder = parent::createEntityFormBuilder($entity, $view);
        $formBuilder->add('detailBondeLs', CollectionType::class, [
            'entry_type' => DetailBondeL::class,
            'allow_delete' => true,
            'allow_add' => true,
            'by_reference' => false,
        ])->add('etat', EntityType::class, [
            'class' => 'App\Entity\Referentiels',
            "attr" => ["class" => "form-control select2", "data-widget" => "select2"],
            'by_reference' => true,
            'query_builder' => function (ReferentielsRepository $er) {
                return $er->getByCode("etat-livraison");
            },

        ])->add('client', Client::class, [
        ]);
        return $formBuilder;
    }

    protected function createBondeCEntityFormBuilder($entity, $view)
    {

        $formBuilder = parent::createEntityFormBuilder($entity, $view);
        $formBuilder->add('detailBondeCs', CollectionType::class, [
            'entry_type' => DetailBondeC::class,
            'allow_delete' => true,
            'allow_add' => true,
            'by_reference' => false,
        ]);

        return $formBuilder;
    }

    protected function createProduitEntityFormBuilder($entity, $view)
    {
        $formBuilder = parent::createEntityFormBuilder($entity, $view);
        $formBuilder->add('referentiels', EntityType::class, [
            'class' => 'App\Entity\Referentiels',
            "attr" => ["class" => "form-control select2", "data-widget" => "select2"],
            'by_reference' => true,
            'query_builder' => function (ReferentielsRepository $er) {
                return $er->getByCode("cat-produit");
            },
        ]);
        return $formBuilder;
    }

    protected function createStockEntityFormBuilder($entity, $view)
    {
        $formBuilder = parent::createEntityFormBuilder($entity, $view);
        $formBuilder->add('mesure', EntityType::class, [
            'class' => 'App\Entity\Referentiels',
            "attr" => ["class" => "form-control select2", "data-widget" => "select2"],
            'by_reference' => true,
            'query_builder' => function (ReferentielsRepository $er) {
                return $er->getByCode(Constants::REF_CODE_MESURE);
            },
        ]);
        return $formBuilder;
    }

    public function createClientEntityFormBuilder($entity, $view)
    {
        $formBuilder = parent::createEntityFormBuilder($entity, $view);
        $formBuilder->add('province', EntityType::class, [
            'class' => 'App\Entity\Referentiels',
            "attr" => ["class" => "form-control select2", "data-widget" => "select2"],
            'by_reference' => true,
            'query_builder' => function (ReferentielsRepository $er) {
                return $er->getByCode(Constants::REF_CODE_PROVINCE);
            },
        ]);
        return $formBuilder;
    }


    public function createNewUserEntity()
    {

        return $this->userM->createUser();
    }

    public function persistUserEntity(User $user)
    {

        $encoder = $this->Encoder_factory->getEncoder($user);
        $user->setPassword($encoder->encodePassword($user->getPassword(), $user->getSalt()));

        $this->UserManager->updateUser($user, false);
        parent::persistEntity($user);
        //die(dump($user));
    }

    public function updateUserEntity($user)
    {
        $encoder = $this->Encoder_factory->getEncoder($user);
        $user->setPassword($encoder->encodePassword($user->getPassword(), $user->getSalt()));

        $this->UserManager->updateUser($user, false);
        parent::updateEntity($user);

    }




//    public function persistEntity($entity)
//    {
//        $this->encodePassword($entity);
//        parent::persistEntity($entity);
//    }
//
//    public function updateEntity($entity)
//    {
//        $this->encodePassword($entity);
//        parent::updateEntity($entity);
//    }
//
//    /**
//     * @var UserPasswordEncoderInterface
//     */
//    private $passwordEncoder;
//
//    /**
//     * UserController constructor.
//     *
//     * @param UserPasswordEncoderInterface $passwordEncoder
//     */
//    public function __construct(UserPasswordEncoderInterface $passwordEncoder)
//    {
//        $this->passwordEncoder = $passwordEncoder;
//    }
//
//    public function encodePassword($user)
//    {
//        if (!$user instanceof User) {
//            return;
//        }
//
//        $user->setPassword(
//            $this->passwordEncoder->encodePassword($user, $user->getPassword())
//        );
//    }





    public function exportAction()
    {
        $sortDirection = $this->request->query->get('sortDirection');
        if (empty($sortDirection) || !in_array(strtoupper($sortDirection), ['ASC', 'DESC'])) {
            $sortDirection = 'DESC';
        }
        $queryBuilder = $this->createListQueryBuilder(
            $this->entity['class'],
            $sortDirection,
            $this->request->query->get('sortField'),
            $this->entity['list']['dql_filter']
        );
        return $this->csvExporter->getResponseFromQueryBuilder(
            $queryBuilder,
            BondeL::class,
            'BondeL.csv'
        );
    }
}
