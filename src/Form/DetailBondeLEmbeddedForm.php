<?php
namespace App\Form;

use App\Entity\DetailBondeL;
use App\Entity\Produit;
use App\Entity\Referentiels;
use App\Repository\ProduitRepository;
use App\Repository\ReferentielsRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;
class DetailBondeLEmbeddedForm extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('produit', EntityType::class, [
                'class' => Produit::class,
                "attr" => ["class" => "form-control select2", "data-widget" => "select2"],
                'by_reference' => true,
                'choice_label' => 'libelle',
                'query_builder' => function(ProduitRepository $repo) {
                    return $repo->createQueryBuilder('produit');
                }
            ])
            ->add('mesure', EntityType::class, [
                'class' => Referentiels::class,
                "attr" => ["class" => "form-control select2", "data-widget" => "select2"],
                'by_reference' => true,
                'choice_label' => 'libelle',
                'query_builder' => function (ReferentielsRepository $er) {

                    return $er->getByCode("pointure-produit");
                },
            ])
            ->add('qte')
            ->addEventListener(
                FormEvents::POST_SET_DATA,
                array($this, 'onPostSetData')
            )
        ;
    }
    public function onPostSetData(FormEvent $event)
    {
        if ($event->getData() && $event->getData()->getId()) {
            $form = $event->getForm();
             //unset($form['produit']);
        }
    }
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => DetailBondeL::class
        ]);
    }
}
