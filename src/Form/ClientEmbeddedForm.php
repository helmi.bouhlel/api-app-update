<?php
namespace App\Form;

use App\Controller\AdminController;
use App\Entity\Client;
use App\Repository\ReferentielsRepository;
use App\Service\Constants;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ButtonType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;
class ClientEmbeddedForm extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $builder
            ->add('nom', TextType::class)
            ->add('prenom', TextType::class,['label' => 'Nom facebook'])
            ->add('tel1', TextType::class)
            ->add('tel2', TextType::class)
            ->add('adresse', TextType::class)
            ->add('province', EntityType::class, [
            'class' => 'App\Entity\Referentiels',
            "attr" => ["class" => "form-control select2", "data-widget" => "select2"],
            'by_reference' => true,
            'query_builder' => function (ReferentielsRepository $er) {
                return $er->getByCode(Constants::REF_CODE_PROVINCE);
            },
        ])
            ->add('save', ButtonType::class, [
                'attr' => ['class' => 'save'],
            ])->addEventListener(
            FormEvents::POST_SET_DATA,
            array($this, 'onPostSetData')
        );
    }
    public function onPostSetData(FormEvent $event)
    {
        if ($event->getData() && $event->getData()->getId()) {
            $form = $event->getForm();
             //unset($form['produit']);
        }
    }
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Client::class
        ]);
    }
}
