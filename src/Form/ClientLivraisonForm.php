<?php
namespace App\Form;

use App\Controller\AdminController;
use App\Entity\Client;
use App\Repository\ReferentielsRepository;
use App\Service\Constants;
use libphonenumber\PhoneNumberFormat;
use Misd\PhoneNumberBundle\Form\Type\PhoneNumberType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ButtonType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;
class ClientLivraisonForm extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $builder
            ->add('nom', TextType::class,[
                "attr" => ["class" => "form-control"],
            ])
            ->add('prenom', TextType::class,[
                "attr" => ["class" => "form-control"],
            ])
                ->add('tel1', TextType::class, [
                "attr" => ["class" => "form-control",'maxlength ' => 8,'minlength ' => 8],
            ])
            ->add('tel2', TextType::class, [
                "attr" => ["class" => "form-control",'maxlength ' => 8,'minlength ' => 8], 'required' => false
            ])
            ->add('adresse', TextType::class,[
                "attr" => ["class" => "form-control"],
            ])->add('pannierJson', HiddenType::class,[
                "attr" => ["class" => "form-control"],
            ])
            ->add('province', EntityType::class, [
            'class' => 'App\Entity\Referentiels',
            'label'=> "Gouvernorat",
            "attr" => ["class" => "w-100", "data-widget" => "select2"],
            'by_reference' => true,
            'query_builder' => function (ReferentielsRepository $er) {
                return $er->getByCode(Constants::REF_CODE_PROVINCE);
            },
        ])->add('Commander', SubmitType::class, [
                'attr' => ['class' => 'btn btn-primary'],
            ])->add('Annuler', ButtonType::class, [
                'attr' => ['class' => 'btn btn-link'],
            ])->addEventListener(
            FormEvents::POST_SET_DATA,
            array($this, 'onPostSetData')
        );
    }
    public function onPostSetData(FormEvent $event)
    {
        if ($event->getData() && $event->getData()->getId()) {
            $form = $event->getForm();
             //unset($form['produit']);
        }
    }
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Client::class
        ]);
    }
}
