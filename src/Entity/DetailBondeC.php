<?php

namespace App\Entity;

use App\Form\DetailBondeCEmbeddedForm;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\DetailBondeCRepository")
 */
class DetailBondeC extends  DetailBondeCEmbeddedForm
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\BondeC", inversedBy="detailBondeCs")
     * @ORM\JoinColumn(nullable=false)
     */
    private $bondeC;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Produit")
     * @ORM\JoinColumn(nullable=false)
     */
    private $produit;

    /**
     * @ORM\Column(type="decimal", precision=10, scale=0)
     */
    private $qte;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Referentiels", inversedBy="detailBondeCs")
     */
    private $mesure;

    private $oldQte;

    /**
     * @ORM\Column(type="float")
     */
    private $prixHT;


    public function __construct()
    {
        $this->prixHT = 0;
    }

    /**
     * @return mixed
     */
    public function getOldQte()
    {
        return $this->oldQte;
    }

    /**
     * @param mixed $oldQte
     */
    public function setOldQte($oldQte): void
    {
        $this->oldQte = $oldQte;
    }



    public function getId(): ?int
    {
        return $this->id;
    }

    public function getBondeC(): ?BondeC
    {
        return $this->bondeC;
    }

    public function setBondeC(?BondeC $bondeC): self
    {
        $this->bondeC = $bondeC;

        return $this;
    }

    public function getProduit(): ?Produit
    {
        return $this->produit;
    }

    public function setProduit(?Produit $produit): self
    {
        $this->produit = $produit;

        return $this;
    }

    public function getQte()
    {
        return $this->qte;
    }

    public function setQte($qte): self
    {
        $this->oldQte = $this->qte;
        $this->qte = $qte;

        return $this;
    }
    public function __toString()
    {
        return strval($this->getProduit()->getLibelle());
    }

    public function getMesure(): ?Referentiels
    {
        return $this->mesure;
    }

    public function setMesure(?Referentiels $mesure): self
    {
        $this->mesure = $mesure;

        return $this;
    }

    public function getPrixHT(): ?float
    {

        return $this->prixHT;
    }


    public function setPrixHT(float $prixHT): self
    {
        $this->prixHT = $prixHT;

        return $this;
    }

}
