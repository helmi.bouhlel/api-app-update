<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Annotation\ApiResource;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
/**
 * @ORM\Entity(repositoryClass="App\Repository\ReferentielsTypeRepository")
 */
class ReferentielsType
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $libelle;


    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Referentiels", mappedBy="referentielsType")
     */
    private $referentiels;


    /**
     * @ORM\OneToMany(targetEntity="App\Entity\ReferentielsType", mappedBy="pere")
     */
    private $fils;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\ReferentielsType", inversedBy="fils")
     * @ORM\JoinColumn(nullable=true)
     */
    private $pere;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $code;


    public function __construct()
    {
        $this->referentiels = new ArrayCollection();
        $this->fils = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getLibelle(): ?string
    {
        return $this->libelle;
    }

    public function setLibelle(string $libelle): self
    {
        $this->libelle = $libelle;

        return $this;
    }


    /**
     * @return Collection|Referentiels[]
     */
    public function getReferentiels(): Collection
    {
        return $this->referentiels;
    }

    public function addReferentiel(Referentiels $referentiel): self
    {
        if (!$this->referentiels->contains($referentiel)) {
            $this->referentiels[] = $referentiel;
            $referentiel->setReferentielsType($this);
        }

        return $this;
    }

    public function removeReferentiel(Referentiels $referentiel): self
    {
        if ($this->referentiels->contains($referentiel)) {
            $this->referentiels->removeElement($referentiel);
            // set the owning side to null (unless already changed)
            if ($referentiel->getReferentielsType() === $this) {
                $referentiel->setReferentielsType(null);
            }
        }

        return $this;
    }


    /**
     * @return Collection|ReferentielsType[]
     */
    public function getFils(): Collection
    {
        return $this->fils;
    }

    public function addFils(ReferentielsType $referentielsType): self
    {
        if (!$this->fils->contains($referentielsType)) {
            $this->fils[] = $referentielsType;
            $referentielsType->setLibelle($this);
        }

        return $this;
    }

    public function removeFils(ReferentielsType $referentielsType): self
    {
        if ($this->fils->contains($referentielsType)) {
            $this->fils->removeElement($referentielsType);
            // set the owning side to null (unless already changed)
            if ($referentielsType->getFils() === $this) {
                $referentielsType->setLibelle(null);
            }
        }
        return $this;
    }


    public function getPere(): ?ReferentielsType
    {
        return $this->pere;
    }

    public function setPere(?ReferentielsType $referentielsType): self
    {
        $this->pere = $referentielsType;
        return $this;
    }

    public function __toString()
    {
        return strval($this->getLibelle());
    }

    public function getCode(): ?string
    {
        return $this->code;
    }

    public function setCode(string $code): self
    {
        $this->code = $code;

        return $this;
    }
}
