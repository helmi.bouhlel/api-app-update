<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\BondeCRepository")
 */
class BondeC
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="decimal", precision=10, scale=0)
     */
    private $prixHT;

    /**
     * @ORM\Column(type="datetime")
     */
    private $dateCre;

    /**
     * @ORM\Column(type="date")
     */
    private $dateC;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\DetailBondeC", mappedBy="bondeC", orphanRemoval=true  ,cascade={"persist"})
     */
    private $detailBondeCs;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Fournisseur", inversedBy="bondeCs" )
     * @ORM\JoinColumn(nullable=false)
     */
    private $fournisseur;


    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $commentaire;


    private $detailBondeCsSupp;

    public function getDetailBondeCsSupp()
    {
        return $this->detailBondeCsSupp;
    }



    public function __construct()
    {
        $this->prixHT = "0";

        $this->detailBondeCs = new ArrayCollection();
        $this->detailBondeCsSupp = new ArrayCollection();
        $this->dateCre = new \DateTime();
        $this->dateC = new \DateTime();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getPrixHT()
    {
        return $this->prixHT;
    }

    public function setPrixHT($prixHT): self
    {
        $this->prixHT = $prixHT;

        return $this;
    }

    public function getDateCre(): ?\DateTimeInterface
    {
        return $this->dateCre;
    }

    public function setDateCre(\DateTimeInterface $dateCre): self
    {
        $this->dateCre = $dateCre;

        return $this;
    }

    public function getDateC(): ?\DateTimeInterface
    {
        return $this->dateC;
    }

    public function setDateC(\DateTimeInterface $dateC): self
    {
        $this->dateC = $dateC;

        return $this;
    }

    /**
     * @return Collection|DetailBondeC[]
     */
    public function getDetailBondeCs(): Collection
    {
        return $this->detailBondeCs;
    }

    public function addDetailBondeC(DetailBondeC $detailBondeC): self
    {
        if (!$this->detailBondeCs->contains($detailBondeC)) {
            $this->detailBondeCs[] = $detailBondeC;
            $detailBondeC->setBondeC($this);
        }

        return $this;
    }

    public function removeDetailBondeC(DetailBondeC $detailBondeC): self
    {
        if ($this->detailBondeCs->contains($detailBondeC)) {
            $this->detailBondeCs->removeElement($detailBondeC);
            // set the owning side to null (unless already changed)
            if ($detailBondeC->getBondeC() === $this) {
                $detailBondeC->setBondeC(null);
                if ($this->detailBondeCsSupp!=null){
                    $this->detailBondeCsSupp->add($detailBondeC);
                }else{
                    $this->detailBondeCsSupp = new ArrayCollection();
                    $this->detailBondeCsSupp->add($detailBondeC);
                }

            }
        }

        return $this;
    }

    public function getFournisseur(): ?fournisseur
    {
        return $this->fournisseur;
    }

    public function setFournisseur(?fournisseur $fournisseur): self
    {
        $this->fournisseur = $fournisseur;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getCommentaire()
    {
        return $this->commentaire;
    }

    /**
     * @param mixed $commentaire
     */
    public function setCommentaire($commentaire): void
    {
        $this->commentaire = $commentaire;
    }
    public function __toString()
    {
        return strval($this->getId());
    }

}
