<?php

namespace App\Entity;

use App\Form\ClientEmbeddedForm;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Misd\PhoneNumberBundle\Validator\Constraints as MisdAssert;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ClientRepository")
 */
class Client extends ClientEmbeddedForm
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $nom;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $prenom;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $tel1;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $tel2;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $adresse;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\BondeL", mappedBy="client")
     */
    private $bondeLs;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Referentiels", inversedBy="clients")
     * @ORM\JoinColumn(nullable=false)
     */
    private $province;


    private $pannierJson;

    public function __construct()
    {
        $this->bondeLs = new ArrayCollection();
    }

    /**
     * @return mixed
     */
    public function getPannierJson()
    {
        return $this->pannierJson;
    }

    /**
     * @param mixed $pannierJson
     */
    public function setPannierJson($pannierJson): void
    {
        $this->pannierJson = $pannierJson;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNom(): ?string
    {
        return $this->nom;
    }

    public function setNom(string $nom): self
    {
        $this->nom = $nom;

        return $this;
    }

    public function getPrenom(): ?string
    {
        return $this->prenom;
    }

    public function setPrenom(string $prenom): self
    {
        $this->prenom = $prenom;

        return $this;
    }

    public function getTel1(): ?string
    {
        return $this->tel1;
    }

    public function setTel1(string $tel1): self
    {
        $this->tel1 = $tel1;

        return $this;
    }

    public function getTel2(): ?string
    {
        return $this->tel2;
    }

    public function setTel2(?string $tel2): self
    {
        $this->tel2 = $tel2;

        return $this;
    }

    public function getAdresse(): ?string
    {
        return $this->adresse;
    }

    public function setAdresse(string $adresse): self
    {
        $this->adresse = $adresse;

        return $this;
    }

    /**
     * @return Collection|BondeL[]
     */
    public function getBondeLs(): Collection
    {
        return $this->bondeLs;
    }

    public function addBondeL(BondeL $bondeL): self
    {
        if (!$this->bondeLs->contains($bondeL)) {
            $this->bondeLs[] = $bondeL;
            $bondeL->setClient($this);
        }

        return $this;
    }

    public function removeBondeL(BondeL $bondeL): self
    {
        if ($this->bondeLs->contains($bondeL)) {
            $this->bondeLs->removeElement($bondeL);
            // set the owning side to null (unless already changed)
            if ($bondeL->getClient() === $this) {
                $bondeL->setClient(null);
            }
        }

        return $this;
    }

    public function getProvince(): ?Referentiels
    {
        return $this->province;
    }

    public function setProvince(?Referentiels $province): self
    {
        $this->province = $province;

        return $this;
    }
    public function __toString()
    {
        return strval($this->getNom() . ' | ' .$this->getTel1(). ' | ' .$this->getProvince()->getLibelle());
    }



}
