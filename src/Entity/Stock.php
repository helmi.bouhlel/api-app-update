<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\StockRepository")
 */
class Stock
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Produit", inversedBy="stocks")
     * @ORM\JoinColumn(nullable=false)
     */
    private $produit;

    /**
     * @ORM\Column(type="decimal", precision=10, scale=0)
     */
    private $qte;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Referentiels")
     */
    private $taille;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Referentiels")
     */
    private $couleur;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Referentiels", inversedBy="stocks")
     */
    private $mesure;

    private $repturer;

    /**
     * @return mixed
     */
    public function getRepturer(): ?string
    {
        if ($this->getProduit()->getStockMin() == null || $this->getProduit()->getStockMin() == 0 ) {
            return "<span class=\"badge badge-warning\">NaN</span>";
        } else if ($this->getQte()-$this->getProduit()->getStockMin() < 0) {
            return "<span class=\"badge badge-danger\">Répturer</span>";
        } else {
            return "<span class=\"badge badge-success\">Disponible</span>";
        }
    }

    /**
     * @param mixed $repturer
     */
    public function setRepturer($repturer): void
    {
        $this->repturer = $repturer;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getProduit(): ?Produit
    {
        return $this->produit;
    }

    public function setProduit(?Produit $produit): self
    {
        $this->produit = $produit;

        return $this;
    }

    public function getQte()
    {
        return $this->qte;
    }

    public function setQte($qte): self
    {
        $this->qte = $qte;

        return $this;
    }

    public function getTaille(): ?referentiels
    {
        return $this->taille;
    }

    public function setTaille(?referentiels $taille): self
    {
        $this->taille = $taille;

        return $this;
    }

    public function getCouleur(): ?referentiels
    {
        return $this->couleur;
    }

    public function setCouleur(?referentiels $couleur): self
    {
        $this->couleur = $couleur;

        return $this;
    }

    public function getMesure(): ?Referentiels
    {
        return $this->mesure;
    }

    public function setMesure(?Referentiels $mesure): self
    {
        $this->mesure = $mesure;

        return $this;
    }

    public function __toString()
    {
        return strval($this->getProduit()->getLibelle());
    }

}
