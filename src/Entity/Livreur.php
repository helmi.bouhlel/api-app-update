<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\LivreurRepository")
 */
class Livreur
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $nom;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $tel1;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $tel2;

    /**
     * @ORM\Column(type="float")
     */
    private $prixLivration;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\BondeL", mappedBy="livreur")
     */
    private $bondeLs;

    public function __construct()
    {
        $this->bondeLs = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNom(): ?string
    {
        return $this->nom;
    }

    public function setNom(string $nom): self
    {
        $this->nom = $nom;

        return $this;
    }

    public function getTel1(): ?string
    {
        return $this->tel1;
    }

    public function setTel1(string $tel1): self
    {
        $this->tel1 = $tel1;

        return $this;
    }

    public function getTel2(): ?string
    {
        return $this->tel2;
    }

    public function setTel2(?string $tel2): self
    {
        $this->tel2 = $tel2;

        return $this;
    }

    public function getPrixLivration()
    {
        return $this->prixLivration;
    }

    public function setPrixLivration($prixLivration): self
    {
        $this->prixLivration = $prixLivration;

        return $this;
    }

    /**
     * @return Collection|BondeL[]
     */
    public function getBondeLs(): Collection
    {
        return $this->bondeLs;
    }

    public function addBondeL(BondeL $bondeL): self
    {
        if (!$this->bondeLs->contains($bondeL)) {
            $this->bondeLs[] = $bondeL;
            $bondeL->setLivreur($this);
        }

        return $this;
    }

    public function removeBondeL(BondeL $bondeL): self
    {
        if ($this->bondeLs->contains($bondeL)) {
            $this->bondeLs->removeElement($bondeL);
            // set the owning side to null (unless already changed)
            if ($bondeL->getLivreur() === $this) {
                $bondeL->setLivreur(null);
            }
        }

        return $this;
    }

    public function __toString()
    {
        return strval($this->getNom());
    }
}
