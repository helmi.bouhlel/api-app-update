<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ReferentielsRepository")
 */
class Referentiels
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $libelle;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $valeur;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\ReferentielsType", inversedBy="referentiels")
     * @ORM\JoinColumn(nullable=false)
     */
    private $referentielsType;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Produit", mappedBy="referentiels")
     */
    private $produits;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\DetailBondeC", mappedBy="mesure")
     */
    private $detailBondeCs;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Stock", mappedBy="mesure")
     */
    private $stocks;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $popular;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Client", mappedBy="province")
     */
    private $clients;



    public function __construct()
    {
        $this->produits = new ArrayCollection();
        $this->detailBondeCs = new ArrayCollection();
        $this->stocks = new ArrayCollection();
        $this->clients = new ArrayCollection();
    }



    public function getId(): ?int
    {
        return $this->id;
    }

    public function getLibelle(): ?string
    {
        return $this->libelle;
    }

    public function setLibelle(string $libelle): self
    {
        $this->libelle = $libelle;

        return $this;
    }

    public function getValeur(): ?string
    {
        return $this->valeur;
    }

    public function setValeur(?string $valeur): self
    {
        $this->valeur = $valeur;

        return $this;
    }

    public function getReferentielsType(): ?ReferentielsType
    {
        return $this->referentielsType;
    }

    public function setReferentielsType(?ReferentielsType $referentielsType): self
    {
        $this->referentielsType = $referentielsType;

        return $this;
    }

    /**
     * @return Collection|Produit[]
     */
    public function getProduits(): Collection
    {
        return $this->produits;
    }

    public function addProduit(Produit $produit): self
    {
        if (!$this->produits->contains($produit)) {
            $this->produits[] = $produit;
            $produit->setReferentiels($this);
        }

        return $this;
    }

    public function removeProduit(Produit $produit): self
    {
        if ($this->produits->contains($produit)) {
            $this->produits->removeElement($produit);
            // set the owning side to null (unless already changed)
            if ($produit->getReferentiels() === $this) {
                $produit->setReferentiels(null);
            }
        }

        return $this;
    }

    public function __toString()
    {
        return strval($this->getLibelle());
    }

    /**
     * @return Collection|DetailBondeC[]
     */
    public function getDetailBondeCs(): Collection
    {
        return $this->detailBondeCs;
    }

    public function addDetailBondeC(DetailBondeC $detailBondeC): self
    {
        if (!$this->detailBondeCs->contains($detailBondeC)) {
            $this->detailBondeCs[] = $detailBondeC;
            $detailBondeC->setMesure($this);
        }

        return $this;
    }

    public function removeDetailBondeC(DetailBondeC $detailBondeC): self
    {
        if ($this->detailBondeCs->contains($detailBondeC)) {
            $this->detailBondeCs->removeElement($detailBondeC);
            // set the owning side to null (unless already changed)
            if ($detailBondeC->getMesure() === $this) {
                $detailBondeC->setMesure(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Stock[]
     */
    public function getStocks(): Collection
    {
        return $this->stocks;
    }

    public function addStock(Stock $stock): self
    {
        if (!$this->stocks->contains($stock)) {
            $this->stocks[] = $stock;
            $stock->setMesure($this);
        }

        return $this;
    }

    public function removeStock(Stock $stock): self
    {
        if ($this->stocks->contains($stock)) {
            $this->stocks->removeElement($stock);
            // set the owning side to null (unless already changed)
            if ($stock->getMesure() === $this) {
                $stock->setMesure(null);
            }
        }

        return $this;
    }

    public function getPopular(): ?bool
    {
        return $this->popular;
    }

    public function setPopular(?bool $popular): self
    {
        $this->popular = $popular;

        return $this;
    }

    /**
     * @return Collection|Client[]
     */
    public function getClients(): Collection
    {
        return $this->clients;
    }

    public function addClient(Client $client): self
    {
        if (!$this->clients->contains($client)) {
            $this->clients[] = $client;
            $client->setProvince($this);
        }

        return $this;
    }

    public function removeClient(Client $client): self
    {
        if ($this->clients->contains($client)) {
            $this->clients->removeElement($client);
            // set the owning side to null (unless already changed)
            if ($client->getProvince() === $this) {
                $client->setProvince(null);
            }
        }

        return $this;
    }


}
