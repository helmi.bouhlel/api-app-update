<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\FournisseurRepository")
 */
class Fournisseur
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $nomPrenom;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $adresse;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $tel1;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $tel2;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\BondeC", mappedBy="fournisseur")
     */
    private $bondeCs;

    public function __construct()
    {
        $this->bondeCs = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNomPrenom(): ?string
    {
        return $this->nomPrenom;
    }

    public function setNomPrenom(string $nomPrenom): self
    {
        $this->nomPrenom = $nomPrenom;

        return $this;
    }

    public function getAdresse(): ?string
    {
        return $this->adresse;
    }

    public function setAdresse(?string $adresse): self
    {
        $this->adresse = $adresse;

        return $this;
    }

    public function getTel1(): ?string
    {
        return $this->tel1;
    }

    public function setTel1(string $tel1): self
    {
        $this->tel1 = $tel1;

        return $this;
    }

    public function getTel2(): ?string
    {
        return $this->tel2;
    }

    public function setTel2(?string $tel2): self
    {
        $this->tel2 = $tel2;

        return $this;
    }

    /**
     * @return Collection|BondeC[]
     */
    public function getBondeCs(): Collection
    {
        return $this->bondeCs;
    }

    public function addBondeC(BondeC $bondeC): self
    {
        if (!$this->bondeCs->contains($bondeC)) {
            $this->bondeCs[] = $bondeC;
            $bondeC->setFournisseur($this);
        }

        return $this;
    }

    public function removeBondeC(BondeC $bondeC): self
    {
        if ($this->bondeCs->contains($bondeC)) {
            $this->bondeCs->removeElement($bondeC);
            // set the owning side to null (unless already changed)
            if ($bondeC->getFournisseur() === $this) {
                $bondeC->setFournisseur(null);
            }
        }

        return $this;
    }

    public function __toString()
    {
        return strval($this->getNomPrenom());
    }
}
