<?php

namespace App\Entity;

use App\Form\DetailBondeLEmbeddedForm;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\DetailBondeLRepository")
 */
class DetailBondeL extends  DetailBondeLEmbeddedForm
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\BondeL", inversedBy="detailBondeLs")
     * @ORM\JoinColumn(nullable=false)
     */
    private $bondeL;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Produit")
     * @ORM\JoinColumn(nullable=false)
     */
    private $produit;

    /**
     * @ORM\Column(type="decimal", precision=10, scale=0)
     */
    private $qte;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Referentiels")
     */
    private $mesure;


    /**
     * @ORM\Column(type="float")
     */
    private $prixHT;

    private $oldQte;
    /**
     * @return mixed
     */
    public function getOldQte()
    {
        return $this->oldQte;
    }

    /**
     * @param mixed $oldQte
     */
    public function setOldQte($oldQte): void
    {
        $this->oldQte = $oldQte;
    }


    public function getId(): ?int
    {
        return $this->id;
    }

    public function getBondeL(): ?BondeL
    {
        return $this->bondeL;
    }

    public function setBondeL(?BondeL $bondeL): self
    {
        $this->bondeL = $bondeL;

        return $this;
    }

    public function getProduit(): ?Produit
    {
        return $this->produit;
    }

    public function setProduit(?Produit $produit): self
    {
        $this->produit = $produit;

        return $this;
    }

    public function getQte()
    {
        return $this->qte;
    }

    public function setQte($qte): self
    {
        $this->oldQte = $this->qte;
        $this->qte = $qte;
        return $this;
    }

    public function getMesure(): ?Referentiels
    {
        return $this->mesure;
    }

    public function setMesure(?Referentiels $mesure): self
    {
        $this->mesure = $mesure;

        return $this;
    }

    public function getPrixHT(): ?float
    {
        return $this->prixHT;
    }

    public function setPrixHT(float $prixHT): self
    {
        $this->prixHT = $prixHT;

        return $this;
    }

    public function __toString()
    {
        return strval($this->getProduit()->getLibelle(). ' | Qte: '. $this->getQte() .' | Msr: '. $this->getMesure());
    }


}
