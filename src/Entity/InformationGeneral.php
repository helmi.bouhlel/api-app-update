<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\InformationGeneralRepository")
 */
class InformationGeneral
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $nomSociete;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $abreviation;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $adresse;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $tel1;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $tel2;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $tel3;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $email;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $pageFB;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $pageInsta;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $imgLogo;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $imgCouverture;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $apropos;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNomSociete(): ?string
    {
        return $this->nomSociete;
    }

    public function setNomSociete(string $nomSociete): self
    {
        $this->nomSociete = $nomSociete;

        return $this;
    }

    public function getAbreviation(): ?string
    {
        return $this->abreviation;
    }

    public function setAbreviation(string $abreviation): self
    {
        $this->abreviation = $abreviation;

        return $this;
    }

    public function getAdresse(): ?string
    {
        return $this->adresse;
    }

    public function setAdresse(?string $adresse): self
    {
        $this->adresse = $adresse;

        return $this;
    }

    public function getTel1(): ?string
    {
        return $this->tel1;
    }

    public function setTel1(string $tel1): self
    {
        $this->tel1 = $tel1;

        return $this;
    }

    public function getTel2(): ?string
    {
        return $this->tel2;
    }

    public function setTel2(string $tel2): self
    {
        $this->tel2 = $tel2;

        return $this;
    }

    public function getTel3(): ?string
    {
        return $this->tel3;
    }

    public function setTel3(?string $tel3): self
    {
        $this->tel3 = $tel3;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getPageFB(): ?string
    {
        return $this->pageFB;
    }

    public function setPageFB(string $pageFB): self
    {
        $this->pageFB = $pageFB;

        return $this;
    }

    public function getPageInsta(): ?string
    {
        return $this->pageInsta;
    }

    public function setPageInsta(?string $pageInsta): self
    {
        $this->pageInsta = $pageInsta;

        return $this;
    }

    public function getImgLogo(): ?string
    {
        return $this->imgLogo;
    }

    public function setImgLogo(string $imgLogo): self
    {
        $this->imgLogo = $imgLogo;

        return $this;
    }

    public function getImgCouverture(): ?string
    {
        return $this->imgCouverture;
    }

    public function setImgCouverture(string $imgCouverture): self
    {
        $this->imgCouverture = $imgCouverture;

        return $this;
    }

    public function getApropos(): ?string
    {
        return $this->apropos;
    }

    public function setApropos(string $apropos): self
    {
        $this->apropos = $apropos;

        return $this;
    }
}
