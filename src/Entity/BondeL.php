<?php

namespace App\Entity;

use App\Service\ValueListTrait;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\BondeLRepository")
 */
class BondeL
{
    use ValueListTrait;
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="decimal", precision=10, scale=0, nullable=true)
     */
    private $prixHT;

    /**
     * @ORM\Column(type="datetime")
     */
    private $dateCre;

    /**
     * @ORM\Column(type="datetime")
     */
    private $dateL;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Client", inversedBy="bondeLs" ,cascade={"persist"})
     */
    private $client;
    private $clientAPI;

    /**
     * @return mixed
     */
    public function getClientAPI()
    {
        return array($this->clientAPI);
    }

    /**
     * @param mixed $clientAPI
     */
    public function setClientAPI($clientAPI): void
    {
        $this->clientAPI = $clientAPI;
    }



    /**
     * @ORM\Column(type="string", length=255,nullable=true)
     */
    private $commentaire;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Referentiels")
     * @ORM\JoinColumn(nullable=true)
     */
    private $etat;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\DetailBondeL", mappedBy="bondeL",orphanRemoval=true  ,cascade={"persist"})
     */
    private $detailBondeLs;
    private $detailBondeLsAPI;
    private $detailBondeLString;

    /**
     * @return array
     */
    public function getDetailBondeLString(): string
    {
        $result = "";
        foreach ($this->detailBondeLs as $bl) {
            $result .= $bl->getProduit()->getLibelle(). " - Qte: " . $bl->getQte() ." - " . $bl->getMesure() ." | \n ";
        }
        return $result;
    }

    /**
     * @param array $detailBondeLString
     */
    public function setDetailBondeLString(array $detailBondeLString): void
    {
        $this->detailBondeLString = $detailBondeLString;
    }


    /**
     * @return mixed
     */
    public function getDetailBondeLsAPI(): array
    {
        return $this->detailBondeLsAPI;
    }

    /**
     * @param mixed $detailBondeLsAPI
     */
    public function setDetailBondeLsAPI($detailBondeLsAPI): void
    {
        $this->detailBondeLsAPI = $detailBondeLsAPI;
    }

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Livreur", inversedBy="bondeLs")
     * @ORM\JoinColumn()
     */
    private $livreur;

    private $detailBondeLsSupp;



    /**
     * @ORM\Column(type="boolean")
     */
    private $livrer;

    public function getDetailBondeLsSupp()
    {
        return $this->detailBondeLsSupp;
    }


    public function __construct()
    {
        $this->livrer = true;
        $this->detailBondeLsAPI = array(DetailBondeL::class);
        $this->detailBondeLs = new ArrayCollection();
        $this->dateCre = new \DateTime();
        $this->dateL = new \DateTime();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getPrixHT()
    {
        return $this->prixHT;
    }

    public function setPrixHT($prixHT): self
    {
        $this->prixHT = $prixHT;

        return $this;
    }

    public function getDateCre(): ?\DateTimeInterface
    {
        return $this->dateCre;
    }

    public function setDateCre(?\DateTimeInterface $dateCre): self
    {
        $this->dateCre = $dateCre;

        return $this;
    }

    public function getDateL(): ?\DateTimeInterface
    {
        return $this->dateL;
    }

    public function setDateL(?\DateTimeInterface $dateL): self
    {
        $this->dateL = $dateL;

        return $this;
    }

    public function getClient(): ?Client
    {
        return $this->client;
    }

    public function setClient(?Client $client): self
    {
        $this->client = $client;

        return $this;
    }

    public function getCommentaire(): ?string
    {
        return $this->commentaire;
    }

    public function setCommentaire(?string $commentaire): self
    {
        $this->commentaire = $commentaire;

        return $this;
    }

    public function getEtat(): ?Referentiels
    {
        return $this->etat;
    }

    public function setEtat(?Referentiels $etat): self
    {
        $this->etat = $etat;

        return $this;
    }

    /**
     * @return Collection|DetailBondeL[]
     */
    public function getDetailBondeLs(): Collection
    {
        return $this->detailBondeLs;
    }

    public function addDetailBondeL(DetailBondeL $detailBondeL): self
    {
        if (!$this->detailBondeLs->contains($detailBondeL)) {
            $this->detailBondeLs[] = $detailBondeL;
            $detailBondeL->setBondeL($this);
        }

        return $this;
    }

    public function removeDetailBondeL(DetailBondeL $detailBondeL): self
    {
        if ($this->detailBondeLs->contains($detailBondeL)) {
            $this->detailBondeLs->removeElement($detailBondeL);
            // set the owning side to null (unless already changed)
            if ($detailBondeL->getBondeL() === $this) {
                $detailBondeL->setBondeL(null);
                if ($this->detailBondeLsSupp!=null){
                    $this->detailBondeLsSupp->add($detailBondeL);
                }else{
                    $this->detailBondeLsSupp = new ArrayCollection();
                    $this->detailBondeLsSupp->add($detailBondeL);
                }

            }
        }

        return $this;
    }

    public function getLivreur(): ?Livreur
    {
        return $this->livreur;
    }

    public function setLivreur(?Livreur $livreur): self
    {
        $this->livreur = $livreur;

        return $this;
    }

    public function __toString()
    {
        return strval($this->getId());
    }



    public function getLivrer(): ?bool
    {
        return $this->livrer;
    }

    public function setLivrer(bool $livrer): self
    {
        $this->livrer = $livrer;

        return $this;
    }
}
