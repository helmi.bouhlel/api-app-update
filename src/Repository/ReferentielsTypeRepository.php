<?php

namespace App\Repository;

use App\Entity\ReferentielsType;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method ReferentielsType|null find($id, $lockMode = null, $lockVersion = null)
 * @method ReferentielsType|null findOneBy(array $criteria, array $orderBy = null)
 * @method ReferentielsType[]    findAll()
 * @method ReferentielsType[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ReferentielsTypeRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, ReferentielsType::class);
    }

    // /**
    //  * @return ReferentielsType[] Returns an array of ReferentielsType objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('r')
            ->andWhere('r.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('r.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?ReferentielsType
    {
        return $this->createQueryBuilder('r')
            ->andWhere('r.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
