<?php

namespace App\Repository;

use App\Entity\DetailBondeL;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method DetailBondeL|null find($id, $lockMode = null, $lockVersion = null)
 * @method DetailBondeL|null findOneBy(array $criteria, array $orderBy = null)
 * @method DetailBondeL[]    findAll()
 * @method DetailBondeL[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class DetailBondeLRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, DetailBondeL::class);
    }

    // /**
    //  * @return DetailBondeL[] Returns an array of DetailBondeL objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('d')
            ->andWhere('d.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('d.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?DetailBondeL
    {
        return $this->createQueryBuilder('d')
            ->andWhere('d.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
