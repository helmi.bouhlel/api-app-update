<?php

namespace App\Repository;

use App\Entity\BondeC;
use App\Entity\DetailBondeC;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use phpDocumentor\Reflection\Types\Boolean;
use phpDocumentor\Reflection\Types\Integer;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method BondeC|null find($id, $lockMode = null, $lockVersion = null)
 * @method BondeC|null findOneBy(array $criteria, array $orderBy = null)
 * @method BondeC[]    findAll()
 * @method BondeC[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class BondeCRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, BondeC::class);
    }

    // /**
    //  * @return BondeC[] Returns an array of BondeC objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('b')
            ->andWhere('b.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('b.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */


    public function updatePrixHT(BondeC $bondeC): ?BondeC
    {
        $detailManager = $this->getEntityManager()->getRepository(DetailBondeC::class);
        $entityManager = $this->getEntityManager();
        $bondeC->setPrixHT($detailManager->getSumPrixByBonde($bondeC->getId()));
        $entityManager->persist($bondeC);
        $entityManager->flush();
        return $bondeC;
    }



}
