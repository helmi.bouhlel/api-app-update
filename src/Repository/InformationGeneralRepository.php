<?php

namespace App\Repository;

use App\Entity\InformationGeneral;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method InformationGeneral|null find($id, $lockMode = null, $lockVersion = null)
 * @method InformationGeneral|null findOneBy(array $criteria, array $orderBy = null)
 * @method InformationGeneral[]    findAll()
 * @method InformationGeneral[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class InformationGeneralRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, InformationGeneral::class);
    }

    // /**
    //  * @return InformationGeneral[] Returns an array of InformationGeneral objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('i')
            ->andWhere('i.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('i.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?InformationGeneral
    {
        return $this->createQueryBuilder('i')
            ->andWhere('i.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
