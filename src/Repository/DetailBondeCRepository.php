<?php

namespace App\Repository;

use App\Entity\DetailBondeC;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use phpDocumentor\Reflection\Types\Integer;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method DetailBondeC|null find($id, $lockMode = null, $lockVersion = null)
 * @method DetailBondeC|null findOneBy(array $criteria, array $orderBy = null)
 * @method DetailBondeC[]    findAll()
 * @method DetailBondeC[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class DetailBondeCRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, DetailBondeC::class);
    }

    // /**
    //  * @return DetailBondeC[] Returns an array of DetailBondeC objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('d')
            ->andWhere('d.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('d.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */


    public function getSumPrixByBonde($value): ?Integer
    {
        return $this->createQueryBuilder('detail_bonde_c')
        ->andWhere('detail_bonde_c.bondeC = :id')
        ->setParameter('id', $value)
        ->select('SUM(detail_bondeC.prixHT) as prixTT')
        ->getQuery()
        ->getOneOrNullResult();
    }

}
