<?php

namespace App\Repository;

use App\Entity\Referentiels;
use App\Entity\ReferentielsType;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\QueryBuilder;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Referentiels|null find($id, $lockMode = null, $lockVersion = null)
 * @method Referentiels|null findOneBy(array $criteria, array $orderBy = null)
 * @method Referentiels[]    findAll()
 * @method Referentiels[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ReferentielsRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Referentiels::class);
    }

    // /**
    //  * @return Referentiels[] Returns an array of Referentiels objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('r')
            ->andWhere('r.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('r.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Referentiels
    {
        return $this->createQueryBuilder('r')
            ->andWhere('r.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */

    public function getByCode (String $code): QueryBuilder
    {

        $repository = $this->getEntityManager()->getRepository(ReferentielsType::class);
        $referentielsType = $repository->findOneBy(['code' => $code]);

        $arrayResult = array();
        array_push($arrayResult, $referentielsType->getId());
        foreach ($referentielsType->getFils() as $referentielsTypeFils) {
            array_push($arrayResult, $referentielsTypeFils->getId());
            $arrFils = $repository->find($referentielsTypeFils->getId())->getFils();
            foreach ($arrFils as $referentielsSousFils) {
                array_push($arrayResult, $referentielsSousFils->getId());
            }
        }
        $result = $this->createQueryBuilder('referentiels')
            ->where(' referentiels.referentielsType in (:sousReferentielsType) ')
            ->setParameter('sousReferentielsType', $arrayResult);

        return $result;
    }
}
