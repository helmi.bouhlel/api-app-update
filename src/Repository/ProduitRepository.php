<?php

namespace App\Repository;

use App\Entity\Produit;
use App\Entity\Referentiels;
use App\Entity\ReferentielsType;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\QueryBuilder;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Produit|null find($id, $lockMode = null, $lockVersion = null)
 * @method Produit|null findOneBy(array $criteria, array $orderBy = null)
 * @method Produit[]    findAll()
 * @method Produit[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ProduitRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Produit::class);
    }

    // /**
    //  * @return Produit[] Returns an array of Produit objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('p.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */


    public function findProduitSolder(): ?array
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.solde IS NOT NULL')
            ->getQuery()
            ->getResult()
        ;
    }

    public function searchText($text): ?array
    {
        $result = $this->createQueryBuilder('p')
            ->andWhere('p.libelle LIKE :searchTerm')
            ->setParameter('searchTerm', '%'.$text.'%')
            ->getQuery()
            ->getResult();
        return  $result;
    }

    public function getByCode (String $id): ?array
    {

        $repository = $this->getEntityManager()->getRepository(Referentiels::class);
        $referentiels = $repository->findBy(['referentielsType' => $id]);

        $arrayResult = array();
        //array_push($arrayResult, $referentielsType->getId());
        foreach ($referentiels as $referentielsTypeFils) {
            array_push($arrayResult, $referentielsTypeFils->getId());

        }
        $result = $this->createQueryBuilder('produit')
            ->where('produit.referentiels in (:arrayResult) ')
            ->setParameter('arrayResult', $arrayResult)->getQuery()->getResult();

        return $result;
    }

}
