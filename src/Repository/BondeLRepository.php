<?php

namespace App\Repository;

use App\Entity\BondeL;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method BondeL|null find($id, $lockMode = null, $lockVersion = null)
 * @method BondeL|null findOneBy(array $criteria, array $orderBy = null)
 * @method BondeL[]    findAll()
 * @method BondeL[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class BondeLRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, BondeL::class);
    }

    // /**
    //  * @return BondeL[] Returns an array of BondeL objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('b')
            ->andWhere('b.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('b.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?BondeL
    {
        return $this->createQueryBuilder('b')
            ->andWhere('b.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
